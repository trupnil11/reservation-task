<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Reservation;
use App\Models\ReservationSetting;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::all();
        return view('reservation',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $requestdata = $request->all();
        $filter = ReservationSetting::where('d',"=",$requestdata['d'])
        ->where('g',"=",$requestdata['g'])
        ->where('tz',"=",$requestdata['tz'])
        ->first();
        if ($filter == null) {
           
           return redirect()->back()->with('message','No Settings Found');
        }
        else
        {
            
            $size = count(collect($request)->get('user_id'));
            if($size <= $filter['n'])
            {
               $requestdata['reservation_timestamp_utc']  = $requestdata['reservation_datetime'];
               $requestdata['user_id'] = json_encode($requestdata['user_id']);
               Reservation::create($requestdata);
               return redirect()->back()->with('message','Reservation Completed');       
            }
            else
            {
                return redirect()->back()->with('message','No Permission');
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
